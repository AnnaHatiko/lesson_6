const express = require('express');
const bodyParser = require('body-parser');
const url = require('url');
const app = express();

var isHeaderKey = function (req, res, next) {
    req.get('Key') ? next() : res.status(401).send('401 Unauthorized');
};

app.use(bodyParser.json());
app.use('/post', isHeaderKey);

app.get("/", function(req, res) {
    res.status(200);
    res.send("Hello, Express.js");
});

app.get("/hello", function(req, res) {
    res.status(200);
    res.send("Hello stranger!");
});

app.get("/hello/:name", function(req, res) {
    res.status(200);
    res.send("Hello, " + req.params.name);
});

app.all("/sub/*", function(req, res) {
    res.send("You requested URI: " + url.format({
        protocol: req.protocol,
        host: req.get('host'),
        pathname: req.originalUrl
    }));
});

app.post("/post", function(req, res) {
    function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }
    if (!isEmpty(req.body)) {
        res.json(req.body);
    }
    else {
        res.status(404);
        res.send("Not found");
    }
});

app.listen(3000);